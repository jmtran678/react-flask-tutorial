import logo from "./logo.svg";
import axios from "axios";
import { useState, useEffect } from "react";
import "./App.css";

function App() {
  const [img, setImg] = useState(null);
  const [qbID, setQbID] = useState(0);
  const api_url = "http://localhost:5000";
  useEffect(() => {
    make_flask_call();
  }, [qbID]);

  const make_flask_call = () => {
    const player_url = api_url + "/player/" + qbID;
    console.log(player_url);
    axios
      .get(player_url)
      .then(function (response) {
        // handle success
        const res = response.data;
        setImg({
          name: res.name,
          src: res.src,
        });
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  };

  function getQB(id) {
    setQbID(id);
  }

  return (
    <div className="App">
      <div>{img !== null && <img src={img.src} alt="QB"></img>}</div>

      <div>
        <button onClick={() => getQB(1)}>Elite Quarterback</button>
        <button onClick={() => getQB(2)}>Mid Quarterback</button>
      </div>
    </div>
  );
}

export default App;
