from flask import Flask
from flask_cors import CORS

app = Flask(__name__)

CORS(app)

names = ["NFL", "Jalen Hurts", "Dak Prescott"]
images = [
    "https://upload.wikimedia.org/wikipedia/en/thumb/a/a2/National_Football_League_logo.svg/800px-National_Football_League_logo.svg.png",
    "https://img.texasmonthly.com/2023/02/jalen-hurts-texas-football-roots.jpg?auto=compress&crop=faces&fit=crop&fm=jpg&h=1400&ixlib=php-3.3.1&q=45&w=1400",
    "https://insidethestar.com/wp-content/uploads/2023/06/mario-herrera-jr-_cowboys-news_dak-prescott-once-again-finds-himself-in-elite-company-5.jpg",
]


@app.route("/")
def football():
    response = {
        "name": names[0],
        "src": images[0],
    }
    return response


@app.route("/player/<int:id>")
def player(id):
    response = {
        "name": names[id],
        "src": images[id],
    }
    return response


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
